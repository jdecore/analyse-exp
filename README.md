# Analyse Prédictive

Les scripts se trouvent dans le dossier AP/synthese

# kmeans.py
Ce script se lance avec 3 paramètres : 
- le chemin vers le fichier de données sur lequel on veut appliquer l'algorithme
- "2" ou "3" en fonction de si ce sont des données 2D ou 3D
- le nombre de clusters souhaités 

Exemple d'utilisation:
On souhaite tester notre script sur un fichier de données 2D "exemple.data" avec 4 clusters :
`python kmeans.py datasets/exemple.data 2 4`

# agglo.py
Ce script a les mêmes 3 premiers paramètres que kmeans.py, mais possède un 4e paramètre pour le linkage choisi

Exemple d'utilisation:
On souhaite tester notre script sur un fichier de données 2D "exemple.data" avec 4 clusters et le linkage "ward" :
`python agglo.py datasets/exemple.data 2 4 ward`

# dbscan.py
Paramètres : 
chemin dataset | 2 ou 3 pour 2d ou 3d | eps | min_samples

Exemple d'utilisation:
On souhaite tester notre script sur un fichier de données 2D "exemple.data" avec eps = 0.3 et min_samples = 15 : 
`python dbscan.py datasets/exemple.data 2 0.3 15`

# strong_dbscan.py (HDBSCAN)
Paramètres : 
chemin dataset | 2 ou 3 pour 2d ou 3d | min_cluster_size

Exemple d'utilisation:
On souhaite tester notre script sur un fichier de données 2D "exemple.data" avec min_cluster_size = 30 : 
`python strong_dbscan.py datasets/exemple.data 2 30`
