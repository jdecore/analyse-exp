from scipy.io import arff
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from sklearn.cluster import kmeans_plusplus
from sklearn.cluster import DBSCAN
from sklearn import metrics 
import time 
import sys 

data = np.loadtxt(sys.argv[1])

x2= []
y2= []
z2= []
data2=[]
score = -1

#print(data)

if sys.argv[2] == "2":
    for (x,y) in data:
        x2.append(x)
        y2.append(y)
        temp = [x,y]
        data2.append(temp)


    start = time.time()
    clustering = DBSCAN(eps=float(sys.argv[3]), min_samples=int(sys.argv[4])).fit(data2)
    end = time.time()
    elapsed = end - start
    print('Temps d\'exécution ,' +str(elapsed)) 
    print(metrics.silhouette_score(data2, clustering.labels_, metric='euclidean'))

    colors = clustering.labels_
    plt.scatter(x2, y2, c=colors, s=5)
    
elif sys.argv[2] == "3":
    for (x,y, z) in data:
        x2.append(x)
        y2.append(y)
        z2.append(z)
        temp = [x,y,z]
        data2.append(temp)
        
    start = time.time()
    clustering = DBSCAN(eps=float(sys.argv[3]), min_samples=int(sys.argv[4])).fit(data2)
    end = time.time()
    elapsed = end - start
    print('Temps d\'exécution ,' +str(elapsed)) 
    #print(metrics.silhouette_score(data2, clustering.labels_, metric='euclidean'))
    
    colors = clustering.labels_
    ax = plt.axes(projection='3d')
    ax.scatter3D(x2, y2, z2, c=colors, s=5)
    
else:
    print("Rentrer 2 ou 3 pour la dimension")
    
plt.show()



