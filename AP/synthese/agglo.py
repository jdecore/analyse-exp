from scipy.io import arff
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from sklearn.cluster import kmeans_plusplus
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn import metrics 
import time 
import sys 


#@TODO
#Rajouter et adapter code pour arguments "nom du fichier", "2d ou 3d", "n_clusters_max", "linkage type"

data = np.loadtxt(sys.argv[1])

x2= []
y2= []
z2= []
data2=[]
score = -1
nmax = int ( sys.argv[3])
link = sys.argv[4]

if sys.argv[2] == "2":
    for (x,y) in data:
        x2.append(x)
        y2.append(y)
        temp = [x,y]
        data2.append(temp)

    for i in range(2,nmax):
        start = time.time()
        clustering = AgglomerativeClustering(linkage=link, n_clusters=i).fit(data2)
        end = time.time()
        elapsed = end - start
        print('Temps d\'exécution pour ' +str(i)+' clusters : '+str(elapsed))
        silhouette = metrics.silhouette_score(data2, clustering.labels_, metric='euclidean') 
        print(silhouette)
        if silhouette > score:
            score = silhouette
            colors = clustering.labels_

    print('Score le plus élevé :' + str(score))
    
    plt.scatter(x2, y2, c=colors, s=5)

elif sys.argv[2] == "3":
    for (x,y, z) in data:
        x2.append(x)
        y2.append(y)
        z2.append(z)
        temp = [x,y,z]
        data2.append(temp)




    for i in range(2,nmax):
        start = time.time()
        clustering = AgglomerativeClustering(linkage=link, n_clusters=i).fit(data2)
        end = time.time()
        elapsed = end - start
        print('Temps d\'exécution pour ' +str(i)+'clusters : '+str(elapsed))
        silhouette = metrics.silhouette_score(data2, clustering.labels_, metric='euclidean') 
        print(silhouette)
        if silhouette > score:
            score = silhouette
            colors = clustering.labels_

    print('Score le plus élevé :' + str(score))
    
    ax = plt.axes(projection='3d')
    ax.scatter3D(x2, y2, z2, c=colors, s=5)

else:
    print("Rentrer 2 ou 3 pour la dimension")


plt.show()



