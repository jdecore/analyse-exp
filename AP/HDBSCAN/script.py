from scipy.io import arff
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from sklearn.cluster import kmeans_plusplus
from sklearn.cluster import DBSCAN
from sklearn import metrics 
import time 
import hdbscan

data = arff.loadarff(open("long1.arff",'r'))[0]

x2= []
y2= []
data2=[]
n=len(data)

#print(data)

for (x,y,z) in data:
    x2.append(x)
    y2.append(y)
    temp = [x,y]
    data2.append(temp)


#print(data2)
n_x = np.array(x2)
n_y = np.array(y2)

#plt.plot(n_x,n_y, "o")
#plt.show()

#for i in range(2,9):
start = time.time()
clustering = hdbscan.HDBSCAN(min_cluster_size=5).fit(data2)
end = time.time()
elapsed = end - start
print('Temps d\'exécution ,'+ str(elapsed)) 
print(metrics.silhouette_score(data2, clustering.labels_, metric='euclidean'))

colors = clustering.labels_

plt.scatter(x2, y2, c=colors, s=5)
plt.show()



