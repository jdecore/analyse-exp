from scipy.io import arff
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from sklearn.cluster import kmeans_plusplus
from sklearn.cluster import DBSCAN
from sklearn import metrics 
import time 
import hdbscan

data = np.loadtxt("dataset/zgn.data")

x2= []
y2= []
data2=[]
score = -1

#print(data)

for (x,y) in data:
    x2.append(x)
    y2.append(y)
    temp = [x,y]
    data2.append(temp)


#for i in range(2,9):
start = time.time()
clustering = hdbscan.HDBSCAN(min_cluster_size=30).fit(data2)
end = time.time()
elapsed = end - start
print('Temps d\'exécution ,'+ str(elapsed)) 
print(metrics.silhouette_score(data2, clustering.labels_, metric='euclidean'))

colors = clustering.labels_

plt.scatter(x2, y2, c=colors, s=5)
plt.show()



