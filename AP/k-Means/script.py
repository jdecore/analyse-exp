from scipy.io import arff
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from sklearn.cluster import kmeans_plusplus
from sklearn.cluster import KMeans
from sklearn import metrics 
import time 


#Formatage des données
data = arff.loadarff(open("2d-10c.arff",'r'))[0]

x2= []
y2= []
data2=[]
n=len(data)
score = -1


for (x,y,z) in data:
    x2.append(x)
    y2.append(y)
    temp = [x,y]
    data2.append(temp)


for i in range(2,10):
    start = time.time()
    kmeans = KMeans(n_clusters=i, init='random').fit(data2)
    end = time.time()
    elapsed = end - start
    print('Temps d\'exécution pour ' +str(i)+'clusters : '+str(elapsed))
    silhouette = metrics.silhouette_score(data2, kmeans.labels_, metric='euclidean') 
    print(silhouette)
    if silhouette > score:
        score = silhouette
        colors = kmeans.labels_

print('Score le plus élevé :' + str(score))

#TODO Choisir les labels correspondants au score de silhouette le plus élevé
#colors = kmeans.labels_

plt.scatter(x2, y2, c=colors, s=5)
plt.show()

